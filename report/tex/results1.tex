\subsection{Results}
\label{se:res1}
In this section, we present the results from the first round. Following the WMT criteria \citep{Barrault20}, we grouped systems
together into clusters according to which systems significantly outperformed all others in lower ranking clusters, according to ART (see \cref{se:eval1}).

Overall, \emph{multilingual} and \emph{transfer learning} approaches yielded the best results for all languages pairs in the constrained category. In fact, except for English{\textendash}German (in which they shared the same ranking), \emph{PROMT}'s multilingual approach{\textemdash}which was the only multilingual system trained for all language pairs{\textemdash}achieved the best results in all cases. This approach also used a smaller vocabulary and \emph{SentencePiece} instead of BPE.

In general, the differences from one position to the next one were of a few points (according to both metrics), with a case (English{\textendash}French) in which there are two points of difference (according to BLEU) between the first and last approaches of the same ranking. Our baselines worked well as delimiters: more sophisticated approaches generally ranked above our Transformer baselines, while the rest ranked either between them or below the RNN baselines. Moreover, the RNN baselines established the limit before a significant drop in translation quality between approaches of one position in the ranking and the next position (sometimes it is the exact limit, while other times there is a cluster above it of a similar quality).

Regarding the unconstrained category, it had less participation than the constrained one. With an exemption (\emph{ETRANSLATION}'s approaches based on their WMT system \citep{Oravecz20} yielded the best results for English{\textendash}German), \emph{PROMT}'s multilingual approach achieved the best results for all language pairs. In general, approaches were similar to the constrained ones but using additional external data. Additionally, due to the use of external data, the best unconstrained systems yielded around 10 BLEU points and 7 ChrF points of improvement compared to the best constrained systems for each language pair.

\subsubsection{English{\textendash}German}\ \\
\cref{ta:de1} presents the results for the English{\textendash}German language pair. 12 different systems from 6 participants were submitted to the constrained category, and 4 different systems from 3 participants were submitted to the unconstrained.

\begin{table}[!h]
	\caption{Results of the English{\textendash}German language pair, divided by categories. Systems are ranked according to BLEU. Lines indicate clusters according to ART. Systems within a cluster are considered tied and, thus, are ranked equally.}
	\label{ta:de1}
	\centering
	\begin{tabular}{c c c c c c}
		\toprule
		 & \textbf{Rank} & \textbf{Team} & \textbf{Description} & \textbf{BLEU [$\uparrow$]} & \textbf{chrF [$\uparrow$]} \\
		\cmidrule(lr){2-2}\cmidrule(lr){3-3}\cmidrule(lr){4-4}\cmidrule(lr){5-5}\cmidrule(lr){6-6}
		\parbox[t]{3mm}{\multirow{14}{*}{\rotatebox[origin=c]{90}{\small Constrained}}} & \multirow{4}{*}{1} & CUNI-MT & transfer2 & $31.6$ & $0.600$ \\
		 & & CUNI-MT & base & $31.4$ & $0.596$ \\
		 & & CUNI-MT & transfer1 & $31.3$ & $0.595$ \\
		 & & PROMT & multilingual & $31.1$ & $0.599$ \\
		\cmidrule(lr){2-6}
		& 2 & ETRANSLATION & basetr & $30.4$ & $0.593$ \\
		\cmidrule(lr){2-6}
		& \multirow{2}{*}{3} & CUNI-MT & transfer2 & $29.8$ & $0.584$ \\
		 & & LC & multilingual & $29.5$ & $0.584$ \\
		\cmidrule(lr){2-6}
		& \multirow{2}{*}{4} & Baseline & transformer & $28.1$ & $0.573$ \\
		 & & LC & transformer & $26.7$ & $0.556$ \\
		\cmidrule(lr){2-6}
		& 5 & TARJAMA-AI & base3 & $25.6$ & $0.564$ \\
		\cmidrule(lr){2-6}
		& 6 & TARJAMA-AI & base2 & $25.0$ & $0.559$ \\
		\cmidrule(lr){2-6}
		& 7 & CUNI-MTIR & r1 & $19.7$ & $0.494$ \\
		\cmidrule(lr){2-6}
		& \multirow{2}{*}{8} & Baseline & RNN & $17.9$ & $0.479$ \\
		 & & TARJAMA-AI & base & $17.7$ & $0.488$ \\
		 \cmidrule(lr){2-6}\morecmidrules\cmidrule(lr){2-6}
		\parbox[t]{3mm}{\multirow{4}{*}{\rotatebox[origin=c]{90}{\small Unconstrained}}} & 1 & ETRANSLATION & wmtfinetune & $44.4$ & $0.686$ \\
		 \cmidrule(lr){2-6}
		 & 2 & ETRANSLATION & wmt & $44.1$ & $0.683$ \\
		 \cmidrule(lr){2-6}
		 & 3 & PROMT & transformer & $41.2$ & $0.666$ \\
		 \cmidrule(lr){2-6}
		 & 4 & CUNI-MTIR & r1 & $20.0$ & $0.499$ \\
		\bottomrule
	\end{tabular}
\end{table}

The best results for the constrained category were achieved by three of \emph{CUNI-MT}'s systems and \emph{PROMT} (who submitted a single system for this language pair). Their approaches were based on transfer learning, standard NMT with back-translation and multilingual NMT. The next best results were achieved by \emph{ETRANSLATION}'s system, which used a transfer learning approach. Then, we have another of \emph{CUNI-MT}'s transfer learning approaches and \emph{LC}'s multilingual approach. On fourth position we have our Transformer baseline and \emph{LC}'s Transformer approach. On fifth and sixth positions are \emph{TARJAMA-AI}'s approaches based on tagged back-translation and combining all language pairs (adding a special token to all sentences except the ones from English{\textendash}German). Next we have \emph{CUNI-MTIR}'s Transformer approach. Finally, our RNN baseline and \emph{TARJAMA-AI}'s NMT approach (they did not specify the architecture they used to train their system) placed last.


For the unconstrained category, the best results were achieved by \emph{ETRANSLATION}'s WMT system fine-tuned with the in-domain data\footnote{We are waiting for their report to know more details about their approach.}. Second position was for \emph{ETRANSLATION}'s WMT system. At third place, we have \emph{PROMT}'s multilingual system. Finally, we have \emph{CUNI-MTIR}'s Transformer approach. It is worth noting how \emph{ETRANSLATION}'s WMT system{\textemdash}which has been trained using exclusively out-of-domain data{\textemdash}achieved around 13 BLEU points of improvement over the best constrained system. We discussed this phenomenon during the virtual meeting and came to the conclusion that, despite that we are working in a very specific domain (Covid-19 related documents), the sub-genre of this domain\footnote{Note that the target audience of Covid-19 MLIA is the general public. Thus, while documents are indeed Covid-19 related, they lean towards information aimed at the citizens instead of  scientific or medical documents.} is more closely related to the news domain of WMT than expected.

\subsubsection{English{\textendash}French}\ \\
\cref{ta:fr1} presents the results for the English{\textendash}French language pair. 12 different systems from 8 participants were submitted to the constrained category, and 8 different systems from 4 participants to the unconstrained one.

\begin{table}[!h]
	\caption{Results of the English{\textendash}French language pair, divided by categories. Systems are ranked according to BLEU. Lines indicate clusters according to ART. Systems within a cluster are considered tied and, thus, are ranked equally.}
	\label{ta:fr1}
	\centering
	\begin{tabular}{c c c c c c}
		\toprule
		 & \textbf{Rank} & \textbf{Team} & \textbf{Description} & \textbf{BLEU [$\uparrow$]} & \textbf{chrF [$\uparrow$]} \\
		\cmidrule(lr){2-2}\cmidrule(lr){3-3}\cmidrule(lr){4-4}\cmidrule(lr){5-5}\cmidrule(lr){6-6}
		\parbox[t]{3mm}{\multirow{12}{*}{\rotatebox[origin=c]{90}{\small Constrained}}} & 1 & PROMT & multilingual & $49.6$ & $0.711$ \\
		\cmidrule(lr){2-6}
		& \multirow{8}{*}{2} & ETRANSLATION & small & $49.1$ & $0.707$ \\
		 & & LC & multilingual & $49.0$ & $0.705$ \\
		 & & LC & transformer & $48.9$ & $0.703$ \\
		 & & CUNI-MT & base & $48.4$ & $0.703$ \\
		 & & CUNI-MT & multiling & $48.0$ & $0.700$ \\
		 & & ETRANSLATION & big & $47.4$ & $0.695$ \\
		 & & Baseline & transformer & $47.3$ & $0.693$ \\
		 & & CUNI-MT & transfer2 & $47.1$ & $0.693$ \\
		\cmidrule(lr){2-6}
		& 3 & LIMSI & trans & $43.5$ & $0.660$ \\
		\cmidrule(lr){2-6}
		& 4 & CUNI-MTIR & r1 & $34.9$ & $0.605$ \\
		\cmidrule(lr){2-6}
		& - & Baseline & RNN & $34.3$ & $0.596$ \\
		\cmidrule(lr){2-6}
		& 5 & TARJAMA-AI & base & $26.8$ & $0.567$ \\
		\cmidrule(lr){2-6}
		& 6 & ACCENTURE & mbart & $15.8$ & $0.464$ \\
		\cmidrule(lr){2-6}\morecmidrules\cmidrule(lr){2-6}
		\parbox[t]{3mm}{\multirow{9}{*}{\rotatebox[origin=c]{90}{\small Unconstrained}}} & 1 & PROMT & transformer & $59.5$ & $0.767$ \\
		\cmidrule(lr){2-6}
		& 2 & ETRANSLATION & gen & $52.9$ & $0.742$ \\
		\cmidrule(lr){2-6}
		& 3 & LIMSI & indom & $51.2$ & $0.721$ \\
		\cmidrule(lr){2-6}
		& \multirow{4}{*}{4} & ETRANSLATION & phwt	& $50.1$ & $0.724$ \\
		& & LIMSI & trans & $49.3$ & $0.710$ \\
		& & LIMSI & bert & $49.3$ & $0.703$ \\
		& & LIMSI & mlia & $48.5$ & $0.705$ \\
		\cmidrule(lr){2-6}
		& 5 & ETRANSLATION & eufl & $47.9$ & $0.712$ \\
		\cmidrule(lr){2-6}
		& 6 & CUNI-MTIR & r1 & $33.0$ & $0.590$ \\
		\bottomrule
	\end{tabular}
\end{table}

\emph{PROMT}'s multilingual approach yielded the best results for the constrained category. Second on the ranking are \emph{ETRANSLATION}'s \emph{small} and \emph{big} approaches\footnote{They have yet to provide a description of their approaches.}, \emph{LC}'s multilingual and Transformer approaches, \emph{CUNI-MT}'s back-translation, multilingual and transfer learning approaches and our Transformer baseline. Finally{\textemdash}placing each one on a different rank{\textemdash}we have \emph{LIMSI}'s transfer learning approach, \emph{CUNI-MTIR}'s Transformer approach, our RNN baseline, \emph{TARJAMA-AI}'s NMT approach and \emph{ACCENTURE}'s multilingual bart approach.

The best unconstrained results were achieved by \emph{PROMT}'s multilingual system. Following is \emph{ETRANSLATION}'s \emph{gen} approach. Then, we have \emph{LIMSI}'s approach based on Transformer using in-domain corpora. On fourth place, we have \emph{ETRANSLATION}'s \emph{phwt} approach and \emph{LIMSI}'s approaches based on using out-of-domain corpora{\textemdash}with and without the use of BERT{\textemdash}fine-tuned with the provided data set, and their approach based on training exclusively with the provided data set, but training BPE using additional in-domain corpora. Then, we have \emph{ETRANSLATION}'s \emph{eufl} approach. Finally, we have \emph{CUNI-MTIR}'s Transformer approach.

\subsubsection{English{\textendash}Spanish}\ \\
\cref{ta:es1} presents the results for the English{\textendash}Spanish language pair. 9 different systems from 6 participants were submitted to the constrained category, and only 2 different systems from 2 participants for the unconstrained one.

\begin{table}[!h]
	\caption{Results of the English{\textendash}Spanish language pair, divided by categories. Systems are ranked according to BLEU. Lines indicate clusters according to ART. Systems within a cluster are considered tied and, thus, are ranked equally.}
	\label{ta:es1}
	\centering
	\begin{tabular}{c c c c c c}
		\toprule
		& \textbf{Rank} & \textbf{Team} & \textbf{Description} & \textbf{BLEU [$\uparrow$]} & \textbf{chrF [$\uparrow$]} \\
		\cmidrule(lr){2-2}\cmidrule(lr){3-3}\cmidrule(lr){4-4}\cmidrule(lr){5-5}\cmidrule(lr){6-6}
		\parbox[t]{3mm}{\multirow{11}{*}{\rotatebox[origin=c]{90}{\small Constrained}}} & 1 & PROMT & multilingual & $48.3$ & $0.702$ \\
		\cmidrule(lr){2-6}
		& \multirow{6}{*}{2} & CUNI-MT & transfer1 & $47.9$ & $0.699$ \\
		 & & CUNI-MT & transfer2 & $47.6$ & $0.698$ \\
		 & & LC & multilingual & $47.5$ & $0.695$ \\
		 & & Baseline & transformer & $47.4$ & $0.694$ \\
		 & & CUNI-MT & multiling & $47.3$ & $0.692$ \\
		 & & CUNI-MT & base & $47.3$ & $0.691$ \\
		\cmidrule(lr){2-6}
		& - & Baseline & RNN & $35.6$ & $0.609$ \\
		\cmidrule(lr){2-6}
		& 3 & CUNI-MTIR & r1 & $32.9$ & $0.591$ \\
		\cmidrule(lr){2-6}
		& 4 & TARJAMA-AI & base & $30.9$ & $0.593$ \\
		\cmidrule(lr){2-6}
		& 5 & ACCENTURE & mbart & $17.4$ & $0.474$ \\
		\cmidrule(lr){2-6}\morecmidrules\cmidrule(lr){2-6}
		\parbox[t]{3mm}{\multirow{2}{*}{\rotatebox[origin=c]{90}{\small Uncon.}}} & 1 & PROMT & transformer & 58.2 & 0.762 \\
		\cmidrule(lr){2-6}
		& 2 & CUNI-MTIR & r1 & 32.1 & 0.582 \\
		\bottomrule
	\end{tabular}
\end{table}

\emph{PROMT}'s multilingual approach yielded the best constrained results. Second in the rank we have \emph{CUNI-MT}'s transfer learning, multilingual and back-translation approaches, \emph{LC}'s multilingual approach and our Transformer baseline. Following up is our RNN baseline. Finally, on third, fourth and fifth positions we have \emph{CUNI-MTIR}'s Transformer approach, \emph{TARJAMA-AI}'s NMT approach and \emph{ACCENTURE}'s multilingual bart approach (respectively).

For the unconstrained category, the best results were achieved by \emph{PROMT}'s multilingual system, followed by \emph{CUNI-MTIR}'s Transformer approach.

\subsubsection{English{\textendash}Italian}\ \\
\cref{ta:it1} presents the results for constrained English{\textendash}Italian language pair. 5 different systems from 4 participants were submitted for the constrained category, and a single system to the unconstrained one.

\begin{table}[!h]
	\caption{Results of the English{\textendash}Italian language pair, divided by categories. Systems are ranked according to BLEU. Lines indicate clusters according to ART. Systems within a cluster are considered tied and, thus, are ranked equally.}
	\label{ta:it1}
	\centering
	\begin{tabular}{c c c c c c}
		\toprule
		& \textbf{Rank} & \textbf{Team} & \textbf{Description} & \textbf{BLEU [$\uparrow$]} & \textbf{chrF [$\uparrow$]} \\
		\cmidrule(lr){2-2}\cmidrule(lr){3-3}\cmidrule(lr){4-4}\cmidrule(lr){5-5}\cmidrule(lr){6-6}
		\parbox[t]{3mm}{\multirow{7}{*}{\rotatebox[origin=c]{90}{\small Constrained}}} & 1 & PROMT & multilingual & $29.6$ & $0.585$ \\
		\cmidrule(lr){2-6}
		& \multirow{3}{*}{2} & LC & multilingual & $28.4$ & $0.572$ \\
		 & & CUNI-MT & transfer2 & $28.3$ & $0.574$ \\
		 & & CUNI-MT & multiling & $28.3$ & $0.574$ \\
		\cmidrule(lr){2-6}
		& - & Baseline & transformer & $26.9$ & $0.560$ \\
		\cmidrule(lr){2-6}
		& 3 & TARJAMA-AI & base & $19.2$ & $0.494$ \\
		\cmidrule(lr){2-6}
		& - & Baseline & RNN & $17.0$ & $0.473$ \\
		\cmidrule(lr){2-6}\morecmidrules\cmidrule(lr){2-6}
		{\small Uncon.} & 1 & PROMT & transformer & 38.0 & 0.642 \\
		\bottomrule
	\end{tabular}
\end{table}

\emph{PROMT}'s multilingual approach yielded the best constrained results. Next, sharing position two, we have \emph{LC}'s multilingual approach and \emph{CUNI-MT}'s transfer learning and multilingual approaches. After that, we have our Transformer baseline. On third position we have \emph{TARJAMA-AI}'s NMT approach. Finally, we have our RNN baseline. 

Regarding the unconstrained category, only \emph{PROMT}'s multilingual approach was submitted.

\subsubsection{English{\textendash}Modern Greek}\ \\
\cref{ta:el1} presents the results for English{\textendash}Modern Greek language pair. 3 different systems from 2 participants were submitted for the constrained category, and a single system was submitted to the unconstrained one. Thus, this was the language pair with less participation. According to participant's reports, this was mostly due to Modern Greek using a different alphabet. 

\begin{table}[!h]
	\caption{Results of the English{\textendash}Modern Greek language pair, divided by categories. Systems are ranked according to BLEU. Lines indicate clusters according to ART. Systems within a cluster are considered tied and, thus, are ranked equally.}
	\label{ta:el1}
	\centering
	\begin{tabular}{c c c c c c}
		\toprule
		& \textbf{Rank} & \textbf{Team} & \textbf{Description} & \textbf{BLEU [$\uparrow$]} & \textbf{chrF [$\uparrow$]} \\
		\cmidrule(lr){2-2}\cmidrule(lr){3-3}\cmidrule(lr){4-4}\cmidrule(lr){5-5}\cmidrule(lr){6-6}
		\parbox[t]{3mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{\small Constrained}}} & 1 & PROMT & multilingual & $27.2$ & $0.523$ \\
		\cmidrule(lr){2-6}
		& 2 & CUNI-MT & transfer1 & $24.7$ & $0.496$ \\
		\cmidrule(lr){2-6}
		& \multirow{2}{*}{3} & CUNI-MT & base & $24.1$ & $0.484$ \\
		 & & Baseline & transformer & $22.6$ & $0.471$ \\
		\cmidrule(lr){2-6}
		& - & Baseline & RNN & $12.8$ & $0.365$ \\
		\cmidrule(lr){2-6}\morecmidrules\cmidrule(lr){2-6}
		{\small Uncon.} & 1 & PROMT & transformer & 42.4 & 0.652 \\
		\bottomrule
	\end{tabular}
\end{table}

Once more, \emph{PROMT}'s multilingual approach yielded the best constrained results. Second, we have \emph{CUNI-MT}'s transfer learning approach. On the third position we have \emph{CUNI-MT}'s back-translation approach, which shares cluster with our Transformer baseline. Finally, we have our RNN baseline.

Only \emph{PROMT}'s multilingual approach was submitted to the unconstrained category.

\subsubsection{English{\textendash}Swedish}\ \\
\cref{ta:sv1} presents the results for the English{\textendash}Swedish language pair. 7 different systems from 5 participants were submitted to the constrained category, and two system from two participants to the unconstrained one. 

\begin{table}[!h]
	\caption{Results of the English{\textendash}Swedish language pair, divided by categories. Systems are ranked according to BLEU. Lines indicate clusters according to ART. Systems within a cluster are considered tied and, thus, are ranked equally.}
	\label{ta:sv1}
	\centering
	\begin{tabular}{c c c c c c}
		\toprule
		& \textbf{Rank} & \textbf{Team} & \textbf{Description} & \textbf{BLEU [$\uparrow$]} & \textbf{chrF [$\uparrow$]} \\
		\cmidrule(lr){2-2}\cmidrule(lr){3-3}\cmidrule(lr){4-4}\cmidrule(lr){5-5}\cmidrule(lr){6-6}
		\parbox[t]{3mm}{\multirow{9}{*}{\rotatebox[origin=c]{90}{\small Constrained}}} & \multirow{3}{*}{1} & PROMT & multilingual & $30.7$ & $0.595$ \\
		 & & LC & multilingual & $30.4$ & $0.589$ \\
		 & & CUNI-MT & transfer2 & $30.1$ & $0.590$ \\
		\cmidrule(lr){2-6}
		& 2 & CUNI-MT & transfer & $28.5$ & $0.578$ \\
		\cmidrule(lr){2-6}
		& - & Baseline & transformer & $27.8$ & $0.566$ \\
		\cmidrule(lr){2-6}
		& 3 & CUNI-MT & base & $26.6$ & $0.561$ \\
		\cmidrule(lr){2-6}
		& 4 & CUNI-MTIR & r1 & $25.1$ & $0.541$ \\
		\cmidrule(lr){2-6}
		& - & Baseline & RNN & $19.2$ & $0.481$ \\
		\cmidrule(lr){2-6}
		& 5 & TARJAMA-AI & base & $11.2$ & $0.443$ \\
		\cmidrule(lr){2-6}\morecmidrules\cmidrule(lr){2-6}
		\parbox[t]{3mm}{\multirow{2}{*}{\rotatebox[origin=c]{90}{\small Uncon.}}} &  1 & PROMT & transformer & 41.3 & 0.671 \\
		\cmidrule(lr){2-6}
		& 2 & CUNI-MTIR & r1 & 24.0 & 0.514 \\
		\bottomrule
	\end{tabular}
\end{table}

The best results for the constrained category were yielded by \emph{PROMT}'s multilingual approach, \emph{LC}'s multilingual approach and one of \emph{CUNI-MT}'s transfer learning approaches. The other \emph{CUNI-MT}'s transfer learning approach placed second on the ranking. Next we have our Transformer baseline. Third position is taken by \emph{CUNI-MT}'s back-translation approach. Next we have \emph{CUNI-MTIR}'s Transformer approach. Following up is our RNN baseline. Finally, on fifth position we have \emph{TARJAMA-AI}'s NMT approach.

Regarding the unconstrained category, the best results were achieved by \emph{PROMT}'s multilingual system, followed by \emph{CUNI-MTIR}'s Transformer approach.