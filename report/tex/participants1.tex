\subsection{Participants' Approaches}
\label{se:participants}
In this section, we present the different approaches submitted by the teams that took part in this round.

\subsubsection{PROMT}\ \\
PROMT's approaches consisted in a multilingual model trained using MarianNMT's \citep{mariannmt} Transformer architecture.

For the constrained category, all data was concatenated using de-duplication to one single multilingual corpus to build a 8k SentencePiece\citep{sentencepiece} model for subword segmentation. In addition, a language-specific tag was added to the source side of the parallel sentence pairs (e.g., \emph{$<it>$} token was added to the beginning of the English sentence of the English{\textendash}Italian sentence pair). They also removed all tokens that appeared less than ten times in the combined de-duplicated monolingual corpus from their vocabulary.

For the unconstrained category, all available data mainly from the OPUS\citep{opus} and statmt\footnote{\url{http://www.statmt.org/}.} with the addition of private data harvested from the Internet was added to the training data. A special BPE implementation\citep{molchanov-2019-promt} developed by the team was applied instead of SentencePiece but the authors used SentencePiece in the constrained category as it seemed to work better in low-resource settings. The size of the BPE models and vocabularies varied from 8k to 16k and shared vocabulary was not used (separate BPE models were trained) for the English{\textendash}Greek pair as the two languages have different alphabets.

The participant submitted systems for all the language pairs and constrained and unconstrained categories. PROMT's systems ranked first in all but the English{\textendash}German unconstrained category. The team plans to tune their baseline systems for the second round.

\subsubsection{CUNI-MT}\ \\
This team submitted 3 different approaches to the constrained category: 1) standard Neural Machine Translation (NMT) training with back-translation; 2) transfer learning; and 3) multilingual training.

\begin{enumerate}
\item Their standard NMT approach relied on one bi-directional model (sharing the encoder and decoder for both translation directions) which constantly switched between training and inference mode to produce batches of synthetic sentence pairs, learning from both authentic and synthetic training samples using online back-translation\citep{onlineBT}. The models were trained on BPE units\citep{Sennrich16} with a vocabulary of 30k items.

\item Their second approach consisted of the transfer learning approach proposed by Kocmi and Bojar\citep{cunimttransfer} (one of the participants), who fine-tuned a low-resource child model from a pre-trained high-resource parent model for a different language pair. The subword vocabulary generated from the child and parent language pair corpora was shared. 

The training procedure consisted of first training an NMT model on the parent parallel corpus until it converges. Then, they replaced the training
data with the child corpus. They experimented repeating this procedure several times with the child becoming the parent for either a completely new language (e.g., German $\rightarrow$ English $\rightarrow$ Spanish $\rightarrow$ $\cdots$ ) or for the original parent (e.g., German $\rightarrow$ English $\rightarrow$ German $\rightarrow$ $\cdots$ ). When adding a new language, the joint BPE vocabulary had to be modified by replacing the original parent vocabulary entries with the new children.

\item Their multilingual approach consisted of a model trained to translate from English to French, Italian and Spanish (due to language similarities). During inference, the corresponding embedding of the target language was selected. The BPE vocabulary was extracted from the concatenation of all four corpora, using only unique English sentences to reach a comparable corpus size. 
\end{enumerate}

The training was performed using the XLM\footnote{\url{https://github.com/facebookresearch/XLM}.} toolkit and the vocabulary size was set to 30k. CUNI-MT's systems ranked first for constrained English{\textendash}German and constrained English{\textendash}Swedish.

\subsubsection{CUNI-MTIR}\ \\
This team submitted systems for English into French, German, Swedish and Spanish in both constrained and unconstrained settings. Transformer architecture from MarianNMT \citep{mariannmt} toolkit was used in order to train the models. 

For unconstrained systems, they used UFAL Medical Corpus\footnote{\url{http://ufal.mff.cuni.cz/ufal\_medical\_corpus}.} for training data and then fine-tuned the models with the constrained data.

All the data was tokenized using Khresmoi\footnote{\url{http://www.khresmoi.eu/assets/Deliverables/WP4/KhresmoiD412.pdf}.}'s tokenizer and, then, encoded using BPE with 32K merges.

\subsubsection{Lingua Custodia (LC)}\ \\
LC submissions consisted of a multilingual model able to translate from English to French, German, Spanish, Italian and Swedish; and individual translation models for English{\textendash}German and English{\textendash}French. 

They applied unigram SentencePiece for subword segmentation using a source and target shared vocabulary of 50K for individual models and 70K for multilingual models. Additionally, authors split the numbers character by character. For multilingual models, a language token is added to the source in order to indicate the target language. The English{\textendash}German multilingual model achieved much higher score than the English{\textendash}German single model. This improvement is not shown in the English{\textendash}French model.

The LC system ranked first for constrained English{\textendash}Swedish. For next rounds, they plan to use transfer learning from massive language models.

\subsubsection{LIMSI}\ \\
LIMSI submitted systems to English{\textendash}French constrained and unconstrained categories. BPE with 32K vocabulary units was applied to the constrained system.
They submitted four unconstrained systems: 1) one system build using an external in-domain biomedical corpora; 2) a system first trained on WMT14\footnote{\url{http://www.statmt.org/wmt14/translation-task.html}.} general data and fine-tuned on the shared task's corpus; 3) same as 2) but adding BERT\citep{zhu2020incorporating}; and 4) a system only trained with constrained data but computing the BPE codes using all the external in-domain corpus.

Their systems were trained using Transformer architecture from \textit{fairseq}\footnote{\url{https://fairseq.readthedocs.io/en/latest/models.html}.} (Facebook's seq-2-seq library).


\subsubsection{TARJAMA-AI}\ \\
Tarjama-AI submitted a single system for English to Spanish, German, Italian, French and Swedish constrained category. This system consisted of a model trained with all the language pairs data adding a special token for the non-target languages. Additionally, they oversampled the corpus of the desired target language (i.e., the English{\textendash}Spanish corpus for training the constrained English{\textendash}Spanish, etc).



\subsubsection{E-Translation}\ \\
E-Translation submitted systems for English{\textendash}German and English{\textendash}French language pairs. They used Transformer models from MarianNMT toolkit \citep{mariannmt}.

For constrained English{\textendash}German, they used transfer learning and 12K size vocabulary created using SentencePiece. For the unconstrained category, they submitted their WMT system and a new version of that system, fine-tuned with the constrained data.

They also participated in constrained English{\textendash}French with two systems described as \emph{small} and \emph{big}, and in unconstrained English{\textendash}French with three systems described as \textit{gen}, \textit{phwt} and \textit{eufl}. Their system ranked first for unconstrained English{\textendash}German.

\subsubsection{ACCENTURE}\ \\
This participants' report is missing but they noted in their system's description that they used multilingual BART\citep{mbart}.