# Machine Translation #

This is the repository of the [Machine Translation](http://eval.covid19-mlia.eu/task3/) task at the [Covid-19 MLIA @ Eval](http://eval.covid19-mlia.eu/) community effort. 

### Organisation of the repository ###

The repository is organised as follows:

* `topics`: this folder contains the topics to be used for task.
* `ground-truth`: this folder contains the ground-truth, i.e. the qrels, for the task.
* `report`: this folder contains the rolling technical report describing the overall outcomes of the task, round after round.

Covid-19 MLIA @ Eval runs in *three rounds*.

Therefore, the `topics` and `ground-truth` folders are organized into sub-folders for each round, i.e. `round1`, `round2`, and `round3`.

### License ###

All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). 

![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

